# Introduction to Mathematical Finance

The following git repository contains the LaTeX source code of the lecture notes for the IMF course. 
Two different lecture notes are available. I would recommend maybe using the one in the lecture notes 1 folder as main version, and use the other one to complement it. 

To make a local copy of this repository on your computer, run the commands below in a terminal. 

Step 1: go into the directory where you want to create the repository using the cd bash command

Step 2: make a local copy of the repository using the command 

`git clone git@gitlab.com:bagersey/introduction-to-mathematical-finance.git`

After making modification to your local copy, you should push the modifications to the server using the following commands

`git add .`

`git commit -m "describe the changes made"`

`git push`


*Important:* in order for all of us to maintain the most recent version containing the changes of the other colleagues, you need to run the 

`git pull`

command before starting to make modifications to your local copy. 

If you have any questions, feel free to contact the maintainer of this repository at balint.gersey@math.ethz.ch


