Website: 
https://www.cadmo.ethz.ch/education/thesis/template.html

Next steps

To use it, proceed as follows:

Unpack the zip; you should get a directory called thesis/ with the files in it. (If you used git, you shouldn't need this guide.)
Open thesis.tex in your favourite TeX editor. (There's also a Makefile if you know what that is.)
Read the template, both the typeset version and the source code (there are lots of comments).
Changes

This is a brief changelog, mainly intended for people already using the template since a certain version so they can "upgrade" the layout manually without losing their content. If you used the zip version, the thesis.tex (main file) will indicate the version you started from at the very top starting with v1.2. (If you used the git version, just pull from the 'layout' branch.)

v1.5 (April 8 2014): Declaration of Originality

You can change this manually by adding

\usepackage{pdfpages}
to the used packages, the declaration of originality to the folder, and

\includepdf[pages={-}]{declaration-originality.pdf}
to the thesis.tex file just before the

\end{document}
v1.4 (August 10, 2009): Coloured citation links

You can change this manually by changing 
\usepackage[linkcolor=black,colorlinks=true]{hyperref}
to 
\usepackage[linkcolor=black,colorlinks=true,citecolor=black,filecolor=black]{hyperref}
v1.3 (July 13, 2009): Overfull hboxes

You can fix this by opening layoutsetup.tex and changing the line 
p, li { white-space: pre-wrap; }

\makeheadrule{chapter}{\headwidth}{0pt}
to

\makeheadrule{chapter}{\textwidth}{0pt}
This has no effect on the actual layout, but makes some "Overfull hbox" warnings go away. (There's also an unrelated Makefile change that you don't have to worry about.)

v1.2 (July 9, 2009): 'proof' environment in roman font

You can fix this manually by inserting the marked line near the end of theoremsetup.tex: 
\theoremstyle{nonumberplain}
\theorembodyfont{\normalfont}            %% <--
\theoremsymbol{\ensuremath{\square}}
\newtheorem{proof}{Proof}
  %\newtheorem{beweis}{Beweis}
v1.1 (June 30, 2009): Load 'hyperref' last to avoid clashes with 'varioref'

You can fix this manually by moving 
%% Make document internal hyperlinks wherever possible. (TOC, references)
  \usepackage[linkcolor=black,colorlinks=true]{hyperref}
further down, at least after '\input{extrapackages}' (but the new version of the template just loads it last). 
v1.0 (June 23, 2009): First release available on this website