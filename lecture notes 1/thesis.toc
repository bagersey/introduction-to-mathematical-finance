\changetocdepth {2}
\select@language {english}
\contentsline {chapter}{Contents}{i}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Basic Concepts in a One-Period Model}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Definitions}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Attainability and Completeness}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Arbitrage}{6}{section.1.3}
\contentsline {section}{\numberline {1.4}Martingale Measures}{9}{section.1.4}
\contentsline {section}{\numberline {1.5}Price systems and attainability}{13}{section.1.5}
\contentsline {section}{\numberline {1.6}Returns and CAPM-type relations}{16}{section.1.6}
\contentsline {section}{\numberline {1.7}Superreplication and hedging duality}{19}{section.1.7}
\contentsline {chapter}{\chapternumberline {2}Arbitrage and Martingale Measures}{27}{chapter.2}
\contentsline {section}{\numberline {2.1}Multi-period financial markets}{27}{section.2.1}
\contentsline {section}{\numberline {2.2}Arbitrage basics}{31}{section.2.2}
\contentsline {section}{\numberline {2.3}The Dalang-Morton-Willinger theorem}{38}{section.2.3}
\contentsline {section}{\numberline {2.4}Density processes and EMM}{42}{section.2.4}
\contentsline {section}{\numberline {2.5}Attainability and completeness}{45}{section.2.5}
\contentsline {section}{\numberline {2.6}The optional decomposition theorem}{50}{section.2.6}
\contentsline {section}{\numberline {2.7}The hedging duality}{54}{section.2.7}
\contentsline {chapter}{\chapternumberline {3}Utility maximisation and stochastic control}{57}{chapter.3}
\contentsline {section}{\numberline {3.1}Setup and problem formulation}{57}{section.3.1}
\contentsline {section}{\numberline {3.2}The martingale optimality principle and dynamic programming}{59}{section.3.2}
\contentsline {section}{\numberline {3.3}Example: Independent returns and power utility}{64}{section.3.3}
\contentsline {section}{\numberline {3.4}Utility from terminal wealth}{67}{section.3.4}
\contentsline {section}{\numberline {3.5}Utility from terminal wealth; duality for $\Omega $ finite}{70}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Utility from terminal wealth and duality}{75}{chapter.4}
\contentsline {section}{\numberline {4.1}Basics}{75}{section.4.1}
\contentsline {section}{\numberline {4.2}Abstract formulation and a dual problem}{77}{section.4.2}
\contentsline {section}{\numberline {4.3}Solving the dual problem}{78}{section.4.3}
\contentsline {section}{\numberline {4.4}From dual to primal problem: idea and motivation}{81}{section.4.4}
\contentsline {section}{\numberline {4.5}Auxiliary results}{82}{section.4.5}
\contentsline {section}{\numberline {4.6}Solution of the primal problem}{88}{section.4.6}
\contentsline {section}{\numberline {4.7}A direct solution to the primal problem}{90}{section.4.7}
